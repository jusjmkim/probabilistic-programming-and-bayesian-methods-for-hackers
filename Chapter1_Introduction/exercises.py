import pymc3 as pm
import theano.tensor as tt
import numpy as np
from math import e

# Set up from book
count_data = np.loadtxt("data/txtdata.csv")
n_count_data = len(count_data)

with pm.Model() as model:
    alpha = 1.0/count_data.mean()

    lambda_1 = pm.Exponential("lambda_1", alpha)
    lambda_2 = pm.Exponential("lambda_2", alpha)

    tau = pm.DiscreteUniform("tau", lower=0, upper=n_count_data - 1)

with model:
    idx = np.arange(n_count_data) # Index
    lambda_ = pm.math.switch(tau > idx, lambda_1, lambda_2)
    observation = pm.Poisson("obs", lambda_, observed=count_data)
    step = pm.Metropolis()
    trace = pm.sample(10000, tune=5000,step=step)

lambda_1_samples = trace['lambda_1']
lambda_2_samples = trace['lambda_2']
tau_samples = trace['tau']

######################## Exercise 1 ########################
e_lambda_1 = lambda_1_samples.mean()
e_lambda_2 = lambda_2_samples.mean()

# Output: 17.754424540326657
print("Mean of Lambda 1: " + str(e_lambda_1))
# Output: 22.709416816340866
print("Mean of Lambda 2: " + str(e_lambda_2))

######################## Exercise 2 ########################
percent_change = (lambda_2_samples / lambda_1_samples).mean() - 1
# Output: 28.076406838698965%
print("Percent increase in text-message rates is " + str(percent_change * 100) + "%")

######################## Exercise 3 ########################
lambda_1_tau_less_45 = list()
for i in range(0, len(tau_samples)):
    if tau_samples[i] < 45: lambda_1_tau_less_45.append(lambda_1_samples[i])

e_lambda_1_tau_less_45 = sum(lambda_1_tau_less_45)/len(lambda_1_tau_less_45)

# Output: 17.751401071641865
print("Mean of Lambda 1: " + str(e_lambda_1_tau_less_45))
